<?php

declare(strict_types=1);

namespace App;

class Car extends VehicleInterface
{
    private int $doors;
    private array $features;

    /**
     * Car constructor.
     */
    public function __construct()
    {
    }

    final public function setDoors(): integer
    {
    }

    private function getFeatures(): Array
    {
        return ['wheels', 'engine', 'cup holders'];
    }

    private function getDoors(): integer
    {
        return $this->doors;
    }
}