<?php

declare(strict_types=1);

namespace App;

class App
{

    /**
     * App constructor.
     */
    public function __construct()
    {
        // Get number of doors for theoretical car
        $car = new Car();
        echo $car->getDoors();
    }
}