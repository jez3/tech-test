<?php

namespace App;

/**
 * Interface VehicleInterface
 * @package src
 */
interface VehicleInterface
{
    /**
     * @return int
     */
    public function getDoors(): int;

    /**
     * @return int
     */
    public function setDoors(): int;

    /**
     * @return array
     */
    public function getFeatures(): array;
}