<?php

use App\App;

include_once __DIR__."/vendor/autoload.php";

$app = new App();
$app->handleRequest();